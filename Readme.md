# `pkg2wef`

Transforms the result of `wasm-pack build` into a Wubix Executable File (WEF).

## Why WEF is needed

Wubix encourages writing programs in Rust, which are then compiled to
WebAssembly. However, we can't run the generated WASM binary directly because it
requires a lot of JS glue, and the Wubix kernel can't (and shouldn't) control
them. To solve this, a WEF contains multiple files (thnk of it as a tarball)
needed to run the program.

## The structure of a WEF

First of all, a WEF is gzipped (gzipping a WEF saves about two thirds of space
as our tests showed). If you ungzip a WEF, you'll geet a file with the following
structure:

- The header, in JSON, with this structure:
  - `files: [string, number][]`: a map of files present in the executable.
    The key in this map is the file name and the value is the file's size;
  - `entry: string` is the name of the entry file (it's loaded first).
- A null byte which terminates the header;
- The contents of each file, in the order defined in the `files` map.

## The entry file

The entry file must be a JS file. This file's job is to set `self.executable`
to a promise that resolves with an object of type `{ start(): Promise<void> }`.
To load other files, the entry file can call `self.getFileBlob`, passing the
file's name and receiving the file's blob URL.

When the entry file is executed, the kernel will await `self.executable`, call
`start` and await the returned promise. If it resolves, the process exits with
status code `0`, but if it throws, the kernel sends `SIGILL` to the process.
The process may be able to handle this signal, but execution of `start` can't
continue, so the process with exit with status code `-SIGILL` (`-4`).

Another commonly exposed function is `__librust_init` of type
`fn(args: Vec<Vec<u8>>, env: HashMap<Vec<u8>, Vec<u8>>) -> ()`. If a program
uses `librust`, this function is avaiable automatically. When present, the
kernel will call it before calling `start`. This function will usually set up
process arguments, environment variables and a panic handler.

> **Note:** This section describes the entry file's job in userspace.
> The kernel is special and doesn't follow the rules described in this section,
> instead it exposes other functions and types required to start the kernel.

## How `pkg2wef` works

`pkg2wef` needs to be invoked in the program crate‘s root, where a directory
named `pkg` must be available. `pkg2wef` will run a webpack build inside `pkg`,
but first it will create `index.js` that dynamically imports the program‘s
main script and assigns this promise to `self.executable`. After the
build `pkg2wef` deletes the temporary `index.js`, alters the generated
bundle to get file URLs from `self.getFileBlob`, packages the generated files
into a WEF and outputs it in the program crate‘s root.

Currently `pkg2wef` runs `webpack` in development mode to alter the generated
bundle, and this means that the generated JavaScript is not optimized.

## FAQ (as if anyone ever asked them)

### Can I write pure-JS programs?

That's possible with WEFs, but the interface is not designed with this in mind
at all. So writing programs in pure JS will be inconvinient.

### Is it secure?

Each process is started in its own worker, so neither the linking script nor
the process itself may affect the system any more than it would do via system
calls. The worker can't make direct calls to the kernel or lie about its PID.
Additionally, workers get rid of `localStorage` and `IndexedDB` before any
user-provided code is loaded, so the only way to manipulate them is via
kernel-controlled `/dev/local_storage` and `/dev/indexed_db`. Direct
manipulations with `sessionStorage` are not a concern because the kernel doesn't
make use of it.
