#!/usr/bin/env node
const { Volume, createFsFromVolume } = require(`memfs`);
const { promises: fs } = require(`fs`);
const { gzip } = require(`zlib`);
const { promisify } = require(`util`);
const webpack = require(`webpack`);

const main = async () => {
  const package = JSON.parse(await fs.readFile(`pkg/package.json`, `utf8`));
  console.log(`pkg2wef: running webpack...`);

  await fs.writeFile(
    `pkg/index.js`,
    `self.executable = import("./${package.module}")`
  );

  const outputFileSystem = Object.create(createFsFromVolume(new Volume()));
  outputFileSystem.join = require(`memory-fs/lib/join`);

  const compiler = webpack({
    mode: `development`,
    entry: `./pkg/index.js`,
    output: {
      filename: `${package.name}.js`,
      path: `/`,
    },
    target: `webworker`,
  });
  compiler.outputFileSystem = outputFileSystem;

  const [error, stats] = await new Promise((resolve) => {
    compiler.run((error, stats) => resolve([error, stats]));
  });
  await fs.unlink(`pkg/index.js`);

  if (error) {
    throw error;
  }

  if (
    (stats.errors && stats.errors.length > 0) ||
    (stats.warnings && stats.warnings.length > 0)
  ) {
    stats.errors && stats.errors.forEach((error) => console.error(error));
    stats.warnings && stats.warnings.forEach((error) => console.error(error));
    process.exit(1);
  }

  console.log(`pkg2wef: compiling ${package.name}.wef...`);

  let mainChunk = outputFileSystem.readFileSync(`/${package.name}.js`, `utf8`);

  mainChunk = mainChunk.replace(
    `importScripts(__webpack_require__.p + "" + chunkId + ".${package.name}.js")`,
    `importScripts(self.getFileBlob(chunkId + ".${package.name}.js"))`
  );
  mainChunk = mainChunk.replace(
    /var req \= fetch\(__webpack_require__\.p \+ \"\" \+ (\{\"\.\/pkg\/(?:[\w\d]+)_bg\.wasm\"\:\"(?:[\w\d]+)\"\}\[wasmModuleId\] \+ \"\.module\.wasm\")\);/,
    `var req = fetch(self.getFileBlob($1));`
  );
  mainChunk = Buffer.from(mainChunk);

  const metadata = {
    entry: `${package.name}.js`,
    files: [[`${package.name}.js`, mainChunk.length]],
  };
  const finalContents = [...mainChunk];

  outputFileSystem.readdirSync(`/`).forEach((fileName) => {
    if (fileName == `${package.name}.js`) {
      return;
    }
    const contents = outputFileSystem.readFileSync(`/${fileName}`);
    metadata.files.push([fileName, contents.length]);
    for (const byte of contents) {
      finalContents.push(byte);
    }
  });

  finalContents.unshift(...Buffer.from(JSON.stringify(metadata)), 0);

  const gzipped = await promisify(gzip)(Buffer.from(finalContents), {
    level: 1,
  });
  await fs.writeFile(`${package.name}.wef`, gzipped);

  console.log(`pkg2wef: done`);
};

main();
